import { Component } from "@angular/core";
import { CourseService } from "./course.service";


@Component({
    selector:'course',
    template: `
    <input type="text" [(ngModel)]="title"/><br/>
    {{title | titleCase}}

    `
})
export class CoursesComponent{
    title:string="";
   
}
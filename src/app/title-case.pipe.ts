import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'titleCase'
})
export class TitleCasePipe implements PipeTransform {

  transform(value: string) {
    if(!value)return null;
    let words = value.split(' ');
    for (let i = 0; i < words.length; i++) {debugger;
      let cVal= words[i];
       if(i>0 && this.isPreposition(cVal)) words[i] = cVal.toLowerCase();
        else words[i] = this.makeTitleCase(cVal);
    }
    return words.join(' ');
  }

  private isPreposition(val:string):boolean{
    let prepositon:string[] = [
      'of',
      'the',
      'and'
    ]

    return prepositon.includes(val.toLowerCase());

  }

  private makeTitleCase(val:string):string{
    return val.substring(0,1).toUpperCase()+val.substring(1).toLowerCase();
  }

}

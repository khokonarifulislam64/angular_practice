import { Component, OnInit, Input, Output , EventEmitter } from '@angular/core';

@Component({
  selector: 'favourite',
  templateUrl: './favourite.component.html',
  styleUrls: ['./favourite.component.css']
})
export class FavouriteComponent implements OnInit {
 @Input("is-favourite") isFavourite = false;
 @Output("my-output-change-event") change = new EventEmitter;

  constructor() {
    console.log('value of isFav is :'+this.isFavourite);
   }

  ngOnInit(): void {
    console.log('ngOnInit value of isFav is :'+this.isFavourite);
  }

  onClick(){
    this.isFavourite=!this.isFavourite;
    this.change.emit(this.isFavourite);
  }

}

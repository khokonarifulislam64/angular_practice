import { Component } from "@angular/core";
import { CourseService } from "./course.service";


@Component({
    selector:'course',
    template: `
    <h4>{{"Title: "+title}}</h4>

         <button class="btn btn-primary" [class.active]="isActive"
             [style.backgroundColor]='isActive ? "red" : "blue" '
             (click) = "onSave($event)"
         >Save</button> <br/>

        <ul>
            <li *ngFor=" let course of courses">
                {{course}}
            </li>
        </ul>
    
    `
})
export class CoursesComponentOld1{
    title = 'This is title of courses'
    isActive= false;
    courses:any[];

    onSave(e:any){
        e.stopPropagation();
        console.log(e);
    }

    constructor(service: CourseService){
        this.courses=service.getCourse();
    }
}
import { Component } from "@angular/core";
import { CourseService } from "./course.service";


@Component({
    selector:'course',
    template: `
    Email:
    <!--<input [value]="email" (keyup.enter) = "email = $event.target.value; onPressed()" />  -->
    <input [(ngModel)]="email" (keyup.enter) = "onPressed()" />
    `
})
export class CoursesComponentOld3{

    email="myDemoEmail@gmail.com";
    onPressed(){
        console.log(this.email);
    }
    
}
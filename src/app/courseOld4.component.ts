import { Component } from "@angular/core";
import { CourseService } from "./course.service";


@Component({
    selector:'course',
    template: `
        {{course.title | uppercase | lowercase }} <br/>
        {{course.students | number }} <br/>
        {{course.rating | number:'1.2-2' }} <br/>
        {{course.price | currency:'AUD':true:'3.2-2' }} <br/>
        {{course.releaseDate | date:'shortDate' }} <br/>
    `
})
export class CoursesComponentOld4{

    course={
        title : "This is the title",
        students : 30123,
        rating: 4.9745,
        price: 190.957,
        releaseDate: new Date(2022, 4, 17)
    }
}
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'hello-world';

  post={
    title:"Title",
    pIsFavourite:true
  }

  onFavouriteChange(isFav:boolean){
    console.log("favourite changed: "+isFav);
  }
  
}
